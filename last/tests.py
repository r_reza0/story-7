from django.test import TestCase, Client
from django.contrib.auth.models import User
from . import urls

url_login = urls.views.login
url_signup = urls.views.regist
url_logout = urls.views.logout

# Create your tests here.
class UnitTest(TestCase):
    # Test untuk mengetahui apakah URL telah tersedia
    def setUp(self):
        user = User.objects.create_user(username='john', password='johnpassword')
        user.save()

    def test_url_login_exist(self):
        response = Client().get(url_login)
        self.assertEqual(response.status_code, 200)

    def test_url_register_exist(self):
        response = Client().get(url_signup)
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = self.client.get(url_login)
        self.assertTemplateUsed(response, "login.html")

    def test_register_using_template(self):
        response = self.client.get(url_signup)
        self.assertTemplateUsed(response, "regist.html")

    def test_create_new_user_from_url(self):
        total_user = User.objects.all().count()
        passed_data = {
            'username': 'thisisusername',
            'password1': 'ThisIsAP@ssword123',
            'password2': 'ThisIsAP@ssword123',
        }
        response = self.client.post(url_signup, data=passed_data)
        self.assertEqual(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), total_user + 1)

    def test_success_login_john(self):
        passed_data = {
            'username': 'john',
            'password': 'johnpassword'
        }
        response = self.client.post(url_login, data=passed_data)
        self.assertEquals(response.status_code, 302)


    def test_logout_user(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(url_logout)
        self.assertEquals(response.status_code, 302)
