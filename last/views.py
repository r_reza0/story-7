from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import buatUser
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def regist(request):
    form = buatUser()
    if (request.method =="POST"):
        form = buatUser(request.POST)
        if form.is_valid():
            form.save()
            return redirect('registration')

    response = {'form': form}
    return render(request, 'regist.html', response)

def halaman_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        pw = request.POST.get('pw')

        user = authenticate(request, username = username, password = pw)

        if user is not None:
            login(request,user)
            return redirect('masuk')
    response = {}
    return render(request,'login.html',response)

def keluar(request):
    logout(request)
    return redirect('login')

def masuk(request):
    response = {}
    return render(request,'halo.html',response)

