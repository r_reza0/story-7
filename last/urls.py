from django.urls import path
from . import views

app_name = 'last'

urlpatterns = [
    path('',views.regist, name='registration'),
    path('login',views.halaman_login,name = 'login'),
    path('masuk', views.masuk, name = 'masuk'),
    path('keluar', views.keluar, name ='logout')
]