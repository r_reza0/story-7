from django.test import TestCase,Client

# Create your tests here.
class MainTestCase(TestCase):
    def setUp(self):
        self.c = Client
    def test_root_url_status_200(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    def test_template_yang_dipake(self):
        response = Client().get('')
        self.assertTemplateUsed(response, "accord.html")

