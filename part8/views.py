from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import requests

# Create your views here.
def search(request):
    response = {}
    return render(request,'part8.html',response)

def getdata(request):
    arg = request.GET['q']
    destination = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    ref = requests.get(destination)
    data = json.loads(ref.content)
    return JsonResponse(data, safe=False)