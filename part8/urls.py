from django.urls import path
from . import views

app_name = 'part8'

urlpatterns = [
    path('',views.search, name='search'),
    path('data/',views.getdata, name='data'),
]